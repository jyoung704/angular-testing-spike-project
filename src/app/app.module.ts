import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { EnterIdComponent } from './enter-id/enter-id.component';
import {FormsModule} from '@angular/forms';
import { DisplayIdComponent } from './display-id/display-id.component';
import {UserDataService} from './user-data.service';

@NgModule({
  declarations: [
    AppComponent,
    DisplayIdComponent,
    EnterIdComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [
    UserDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
