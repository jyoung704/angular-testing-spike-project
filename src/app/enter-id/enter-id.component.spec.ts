import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EnterIdComponent} from './enter-id.component';
import {By} from '@angular/platform-browser';
import {UserDataService} from '../user-data.service';
import {FormsModule} from '@angular/forms';

describe('EnterIdComponent', () => {
  let component: EnterIdComponent;
  let fixture: ComponentFixture<EnterIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        EnterIdComponent
      ],
      imports: [
        FormsModule // Note how important this is. NgModel will not work within the tests without it.
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should say hello', () => {
    const greeting = fixture.debugElement.query(By.css('.greeting'));
    // using toContain() instead of equals() prevents easily breakable tests
    // as long as the message says hello, we can change the exact formatting
    // such as removing or adding the exclamation mark
    expect(greeting.nativeElement.textContent.trim()).toContain('Hello');
  });

  it('should address the user after submit', () => {
    const name: string = 'James';

    fixture.detectChanges();
    fixture.whenStable().then(() => {
      const input = fixture.debugElement.query(By.css('#firstName')).nativeElement;
      input.value = name;
      input.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        const button = fixture.debugElement.query(By.css('#submitButton')).nativeElement;
        button.click();
        fixture.whenStable().then(() => {
          fixture.detectChanges();
          const greeting = fixture.debugElement.query(By.css('.greeting'));
          expect(greeting.nativeElement.textContent.trim()).toContain(name);
        });
      });
    });
  });
});
