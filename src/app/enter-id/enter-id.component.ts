import {Component, OnInit} from '@angular/core';
import {UserDataService} from '../user-data.service';

@Component({
  selector: 'app-enter-id',
  templateUrl: './enter-id.component.html',
  styleUrls: ['./enter-id.component.css']
})
export class EnterIdComponent implements OnInit {
  private greeting: string = 'Hello';
  message: string = this.greeting;
  userFirstName: string;
  userId: string;

  constructor(private userDataService: UserDataService) { }

  ngOnInit() {
  }

  getMessage(): string {
    if (this.userDataService.getUserFirstName() === '') {
      this.message = this.greeting + '!';
    } else {
      this.message = this.greeting + ', ' + this.userFirstName + '!';
    }
    return this.message;
  }

  updateUserData(userName: string, userId: string) {
    this.userDataService.setUserFirstName(userName);
    this.userDataService.setUserId(userId);
    this.message = this.getMessage();
  }

  onClick() {
    this.updateUserData(this.userFirstName, this.userId);
  }
}
