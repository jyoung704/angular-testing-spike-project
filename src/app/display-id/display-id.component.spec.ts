import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';

import {DisplayIdComponent} from './display-id.component';
import {UserDataService} from '../user-data.service';
import {By} from '@angular/platform-browser';

describe('DisplayIdComponent', () => {
  let component: DisplayIdComponent;
  let fixture: ComponentFixture<DisplayIdComponent>;
  let testBedService: UserDataService;
  let componentService: UserDataService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DisplayIdComponent
      ],
      providers: [
        UserDataService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayIdComponent);
    componentService = fixture.debugElement.injector.get(UserDataService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display the user ID in the Service', () => {
    const id = 'testId';
    componentService.setUserId(id);

    const userIdElement = fixture.debugElement.query(By.css('#userId')).nativeElement;
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(userIdElement.textContent).toContain(componentService.getUserId());
    });
  });
});
