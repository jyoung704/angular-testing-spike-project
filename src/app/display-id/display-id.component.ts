import { Component, OnInit } from '@angular/core';
import {UserDataService} from '../user-data.service';

@Component({
  selector: 'app-display-id',
  templateUrl: './display-id.component.html',
  styleUrls: ['./display-id.component.css']
})
export class DisplayIdComponent implements OnInit {

  studentID: string;

  constructor(private userDataService: UserDataService) { }

  ngOnInit() {
    this.userDataService.getUser().subscribe((user) => {
      this.studentID = user.userId;
    });
  }
}
