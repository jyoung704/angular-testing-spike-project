import {TestBed} from '@angular/core/testing';

import {UserDataService} from './user-data.service';

describe('UserDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserDataService = TestBed.get(UserDataService);
    expect(service).toBeTruthy();
  });

  it('should update observable when user name changes', () => {
    const service: UserDataService = TestBed.get(UserDataService);
    let newName = 'newname';
    service.setUserFirstName(newName);
    service.getUser().subscribe((user) => {
      expect(user.userFirstName).toEqual(newName);
    });
  });

  it('should update observable when id changes', () => {
    const service: UserDataService = TestBed.get(UserDataService);
    let newId = '9999987';
    service.setUserId(newId);
    service.getUser().subscribe((user) => {
      expect(user.userId).toEqual(newId);
    });
  });
});
