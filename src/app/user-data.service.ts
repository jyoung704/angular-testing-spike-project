import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

class User {
  userFirstName: string;
  userId: string;
}

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  private user = new User();
  private userSource = new BehaviorSubject(this.user);
  private user$ = this.userSource.asObservable();

  constructor() {
  }

  public getUser(): Observable<User> {
    return this.user$;
  }

  public getUserFirstName(): string {
    let name;
    if (this.user.userFirstName == null) {
      name = '';
    } else {
      name = this.user.userFirstName;
    }
    return name;
  }

  public getUserId(): string {
    return this.user.userId;
  }

  public setUserFirstName(name: string) {
    this.user.userFirstName = name;
    this.userSource.next(this.user);
  }

  public setUserId(id: string) {
    this.user.userId = id;
    this.userSource.next(this.user);
  }
}
