import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {UserDataService} from './user-data.service';
import {EnterIdComponent} from './enter-id/enter-id.component';
import {DisplayIdComponent} from './display-id/display-id.component';
import {FormsModule} from '@angular/forms';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        EnterIdComponent,
        DisplayIdComponent
      ],
      providers: [
        UserDataService
      ],
      imports: [
        FormsModule
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'AngularTestingSpike'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('AngularTestingSpike');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('AngularTestingSpike app is running!');
  });
});
